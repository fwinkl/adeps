// Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

var network, nodesView, edgesView, currentPkg;
var packages = {};

/* Server URL for .dot files. Using an absolute URL here allows local
   testing of the web page but using the official dot files. */
const dot_url = 'https://atlas-sw-adeps.web.cern.ch/atlas-sw-adeps/data/'

const path = dot_url + 'main/Athena/';


/*
 * Default options for network graph
 * See https://visjs.github.io/vis-network/docs/network/
 */
var options = {
    layout: {
        randomSeed: 0
    },
    edges: {
        arrows: {
            to: {
                scaleFactor: 0.8
            }
        },
        color: {
            inherit: false
        }
    }
}

// Node colors:
const opt_cpp_node = {
    border: '#2b7ce0', background: '#97c2fc',
    highlight: { border: '#2b7ce9', background: '#97c2fc' }
}

const opt_python_node = {
    border: 'grey', background: 'lightgrey',
    highlight: { border: 'grey', background: 'lightgrey' }
}

const opt_current_node = {
    border: 'DarkRed', background: 'LightCoral',
    highlight: {border: 'DarkRed', background: 'LightCoral'}
}


/*
 * Initialize on page load
 */
function init() {

    // Fill header:
    fetch(path+'release.txt')
        .then(response => response.text())
        .then(data => {
            document.getElementById('release').innerHTML = 'Release: '+data;
        });

    // Populate package list:
    fetch(path+'packages.txt')
        .then(response => response.text())
        .then(data => {
            let list = document.getElementById('packages');
            data.split('\n').forEach(p => {
                if (!p.length) return;
                const item = document.createElement('li');
                const a = document.createElement('a');
                a.href = '#';
                a.text = p.split('/').pop();   // package name
                a.addEventListener('click', (e) => {
                    loadDeps(e.target.text);
                });
                item.appendChild(a);
                list.append(item);
                packages[a.text] = p
            });
        });

    // Event listeners for check/radio boxes:
    document.getElementById('python').addEventListener('input', () => {
        nodesView.refresh();
        edgesView.refresh();
        show();
    });
    document.getElementById('asText').addEventListener('input', show);

    document.getElementsByName('deps').forEach(radio => {
        radio.addEventListener('input', (e) => { loadDeps(currentPkg);});
    });

    // Populate fields from URL
    let params = (new URL(document.location)).searchParams;
    pkg = params.get('package');
    if (pkg) {
        if (params.get('deps')=='clients') {
            document.getElementById('clients').checked = true
        }
        if (params.get('python')>0) {
            document.getElementById('python').checked = true
        }
        if (params.get('asText')>0) {
            document.getElementById('asText').checked = true
        }
        loadDeps(pkg);
    }
}

/*
 * Load dependencies for pkg
 */
async function loadDeps(pkg) {
    if (pkg==null) return;

    currentPkg = pkg;
    const prefix = document.querySelector('input[name="deps"]:checked').value;
    let filename = prefix + '/' + pkg + '.dot';

    // Create network and set event handlers:
    network = new vis.Network(document.getElementById('graph'), {}, options);
    network.on('doubleClick', function(props) {
        loadDeps(props.nodes[0]);    // navigate
    });
    network.once('stabilized', function() {
        network.fit({maxZoomLevel: 1.4});  // fit on screen
    });

    let graphFilter = (item) => {
        return (document.getElementById("python").checked ||
                (!(item.style || item.dashes) ||   // python node/edge
                 currentPkg==item.label));         // never filter ourselves
    };

    let response = await fetch(path+filename);
    const data = await response.text();

    const parsedData = vis.parseDOTNetwork(data);

    // Make sure there is at least one node visible:
    if (parsedData.nodes.length==0) {
        parsedData.nodes = [{id: pkg, label: pkg}];
    }

    // Color nodes and edges:
    parsedData.nodes.forEach(n => {
        n.color = n.style ? opt_python_node : opt_cpp_node;
        if (n.label==currentPkg) n.color = opt_current_node;
    });
    parsedData.edges.forEach(e => {
        e.color = e.dashes ? opt_python_node.border : opt_cpp_node.border;
    });

    nodesView = new vis.DataView(new vis.DataSet(parsedData.nodes), { filter: graphFilter });
    edgesView = new vis.DataView(new vis.DataSet(parsedData.edges), { filter: graphFilter });
    network.setData({nodes: nodesView, edges: edgesView}, false);

    show();
}

/*
 * Show graph or text dependencies
 */
function show() {
    document.getElementById('intro').style.display = 'none';
    const doTxt = document.getElementById('asText').checked;
    document.getElementById('txt').style.display = doTxt ? 'block' : 'none';
    document.getElementById('graph').style.display = doTxt ? 'none' : 'block';
    if (doTxt) txt();
    else draw();
    setPermalink();
}

/*
 * Update permalink
 */
function setPermalink() {
    currentURL = new URL(document.location);
    href = currentURL.origin + '/?' +
        'package=' + currentPkg;

    if (document.getElementById('clients').checked) href += '&deps=clients';
    if (document.getElementById('python').checked)  href += '&python=1';
    if (document.getElementById('asText').checked)  href += '&asText=1';

    document.getElementById('permalink').href = href
}

/*
 * Draw graph
 */
function draw() {

    // For many nodes we disable physics and stabilize manually:
    if (nodesView.length>100) {
        network.setOptions({physics: {enabled: false}});
        network.stabilize(50);
    }
    else {
        network.setOptions({physics: {enabled: true}});
        network.startSimulation();
    }
}

/*
 * Show dependencies as text
 */
function txt() {
    let nodes = [];
    nodesView.forEach(n => {
        nodes.push(n.style ? 'Py:' + n.label : n.label);
    });
    document.getElementById('txt').innerHTML = '<p>' + nodes.sort().join('</br>') + '</p>';
}

/*
 * Filter box
 */
function filter() {
    const filter = document.getElementById('filter').value.toLowerCase();
    const items = document.getElementById('packages').getElementsByTagName('li');
    for (let li of items) {
        if (li.textContent.toLowerCase().indexOf(filter) > -1)
            li.style.display = '';
        else
            li.style.display = 'none';
    }
}
