#!/usr/bin/env bash
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Script to create the .dot files required by the adeps website

if [ $# -ne 1 ]; then
    release="Athena,main,latest"
fi

# Number of parallel jobs
NJOBS=`nproc`
if [ $NJOBS -gt 8 ]; then
    NJOBS=8
fi

# Setup release
export AtlasSetup=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/current/AtlasSetup
source ${AtlasSetup}/scripts/asetup.sh ${release}
rm .asetup.save

mkdir -p ${AtlasBuildBranch}/${AtlasProject} && cd $_

# List of packages and release information
pkgs=`grep -v '#' ${ATLAS_RELEASE_BASE}/${AtlasProject}/${AtlasVersion}/InstallArea/${BINARY_TAG}/packages.txt`
echo "${pkgs}" > packages.txt
echo "$AtlasProject,$AtlasBuildBranch,$AtlasBuildStamp" > release.txt

# Dependencies
mkdir depends && cd $_
acmd.py cmake depends --py --batch ${NJOBS} ${pkgs}
rm -f *.txt
cd ..

# Clients
mkdir clients && cd $_
acmd.py cmake depends --clients --py --batch ${NJOBS} ${pkgs}
rm -f *.txt
cd ..

# Full graph:
acmd.py cmake depends --py --dot --regex '.*' > adeps.dot
