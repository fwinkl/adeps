# ATLAS Athena package dependencies

This repository creates and serves the [Athena package dependency graph webite (adeps)](https://atlas-sw-adeps.web.cern.ch).
The dependency graphs are re-created daily using a scheduled pipeline. The graphs are created using [`acmd.py cmake depends`](https://gitlab.cern.ch/atlas/athena/-/blob/master/Tools/PyUtils/python/scripts/cmake_depends.py) for each package in the release and stored as `.dot` files on the website.

The graph rendering is done using [vis.js Network](https://visjs.org/).

## Local development/testing

1. Clone this repository
2. `cd adeps/www`
3. Start a local web server (e.g. `python -m http.server`) or alternatively serve all files via your CERNBox/EOS website.

The `.dot` files are read from the production server by default. If needed they can be created locally running [bin/adeps_create.py](bin/adeps_create.py).
